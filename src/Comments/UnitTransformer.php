<?php namespace Laravelista\Comments\Comments;

use League\Fractal\TransformerAbstract;

class UnitTransformer extends TransformerAbstract
{

    public function transform($unit)
    {
        return [
            'id' => (int) $unit->id,
            'name' => $unit->name,
            'name' => $unit->slug,
        ];
    }

}
